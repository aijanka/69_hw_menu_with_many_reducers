import React from 'react';
import './MenuItem.css'

const MenuItem = props => {
    // const url = props.image + '.png';
    return (
        <div className="Menu_Item">
            {/*{console.log(props.image)}*/}
            {/*<hr/>*/}
            <h4>{props.name}</h4>
            <img src={props.image} alt='' className='MenuItemImage'/>
            <p>KGS  {props.price}</p>
            <button onClick={props.clicked}>Add to chart</button>
        </div>
        );
}


export default MenuItem;