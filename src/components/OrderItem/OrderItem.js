import React from 'react';

const OrderedItem = props => (
    <div className="OrderItem">
        <h4>{props.name}</h4>
        <p>{props.amount}</p>
        <button onClick={props.delete}>delete item</button>
    </div>
);

export default OrderedItem;