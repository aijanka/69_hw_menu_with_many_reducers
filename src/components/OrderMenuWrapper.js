import React, {Fragment} from 'react';
import MenuItems from "../containers/MenuItems/MenuItems";
import Order from "../containers/Order/Order";

const OrderMenuWrapper = () => (
    <Fragment>
        <MenuItems/>
        <Order/>
    </Fragment>
);

export default OrderMenuWrapper;