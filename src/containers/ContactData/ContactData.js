import React, {Component} from 'react';
import {connect} from 'react-redux';

import './ContactData.css';
// import Button from "../../../components/UI/Button/Button";

import {Redirect} from "react-router-dom";
import Spinner from "../../components/UI/Spinner/Spinner";
import {placeOrder} from "../../store/actions/order";

class ContactData extends Component {
    state = {
        name: '',
        email: '',
        street: '',
        postal: ''
    };

    valueChanged = (event) => {
        this.setState({[event.target.name]: event.target.value});
    };

    orderHandler = event => {
        event.preventDefault();
        this.setState({loading: true});
        const order = {
            ingredients: this.props.ingredients,
            price: this.props.price,
            customer: {
                name: this.state.name,
                email: this.state.email,
                street: this.state.street,
                postal: this.state.postal
            }
        };

        this.props.onPlaceOrder(order);
    };

    render() {
        let form = (
            <form>
                <input className="Input" type="text" name="name" placeholder="Your Name"
                       value={this.state.name} onChange={this.valueChanged}/>
                <input className="Input" type="email" name="email" placeholder="Your Mail"
                       value={this.state.email} onChange={this.valueChanged}/>
                <input className="Input" type="text" name="street" placeholder="Street"
                       value={this.state.street} onChange={this.valueChanged}/>
                <input className="Input" type="text" name="postal" placeholder="Postal Code"
                       value={this.state.postal} onChange={this.valueChanged}/>

                <button clicked={this.orderHandler} btnType="Success">ORDER</button>
            </form>
        );

        if (this.props.loading) {
            form = <Spinner/>;
        }

        if(this.props.ordered) {
            form = <Redirect to='/'/>
        }

        return (
            <div className="ContactData">
                <h4>Enter your Contact Data</h4>
                {form}
            </div>
        );
    }
}

const mapStateToProps = state => {
    return {
        orderedItems: state.order.order
    }
};

const mapDispatchToProps = dispatch => {
    return {
        onPlaceOrder: (order) => dispatch(placeOrder(order)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactData);