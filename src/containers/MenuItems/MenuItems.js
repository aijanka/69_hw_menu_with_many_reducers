import React, {Component} from 'react';
import {connect} from "react-redux";

import Spinner from "../../components/UI/Spinner/Spinner";
import {getMenu} from "../../store/actions/menuItems";
import {orderItem} from "../../store/actions/order";
import MenuItem from "../../components/MenuItem/MenuItem";

import './MenuItems.css';

class MenuItems extends Component {
    componentDidMount() {
        this.props.getMenu();
    }

    render() {
        let menuItems = null;
        if(this.props.menuItems !== null) {
            const keys = Object.keys(this.props.menuItems);
            const items = this.props.menuItems;

            menuItems = keys.map(key => {
                return <MenuItem
                    key={items[key].name}
                    image={items[key].image}
                    name={items[key].name}
                    price={items[key].price}
                    clicked={() => this.props.orderItem(key, items[key].name)}
                />});

        }
        if (this.props.loading) {
            menuItems = <Spinner/>;
        }
        return (
            <div className="MenuItems">
                {menuItems}
            </div>
        );
    }
}

const mapStateToProps = state => ({
    menuItems: state.menuItems.menuItems,
    loading: state.loading
});

const mapDispatchToProps = dispatch => {
    return {
        getMenu: () => dispatch(getMenu()),
        orderItem: (key, name) => dispatch(orderItem(key, name))
    };
};


export default connect(mapStateToProps, mapDispatchToProps)(MenuItems);