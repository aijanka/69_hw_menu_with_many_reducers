import React, {Component} from 'react';
import {connect} from "react-redux";
import OrderedItem from "../../components/OrderItem/OrderItem";
import {placeOrder, reduceOrderedItem} from "../../store/actions/order";

import './Order.css';

class Order extends Component {
    // placeOrder = () => {
    //     this.props.history.push('/contact-data');
    // }

    render() {
        const { orderedItems } = this.props;
        // const orderedItems = this.props.orderedItems;
        let v = null;
        if(Object.keys(orderedItems).length === 0){
            v = <p className='emptyOrder'>Please, make an order!</p>

        } else {
            v = Object.keys(orderedItems).map(key => <OrderedItem  key={key}
                                                                   name={orderedItems[key].name}
                                                                   amount={orderedItems[key].amount}
                                                                   delete={() => this.props.deleteOrderedItem(key)}/>)
        }

        return (
            <div className="Order">
                {v}
                {/*<button onClick={this.placeOrder}>Сделать заказ</button>*/}
            </div>
        )
    }
}

const mapStateToProps = state => ({
    orderedItems: state.order.order
});

const mapDispatchToProps = dispatch => ({
    deleteOrderedItem: name => dispatch(reduceOrderedItem(name)),
    // placeOrder: () => dispatch(placeOrder())
});


export default connect(mapStateToProps, mapDispatchToProps)(Order);
