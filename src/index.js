import React from 'react';
import axios from "axios";
import ReactDOM from 'react-dom';
import {applyMiddleware, combineReducers, compose, createStore} from "redux";
import thunk from 'redux-thunk';
import './index.css';
import App from './App';
import registerServiceWorker from './registerServiceWorker';
import {Provider} from "react-redux";
import menuItems from "./store/reducers/menuItems";
import order from "./store/reducers/order";
// import {BrowserRouter} from "react-router-dom";


const rootReducers = combineReducers({
    menuItems: menuItems,
    order: order
});

const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const store = createStore(rootReducers, composeEnhancers(applyMiddleware(thunk)));

axios.defaults.baseURL = 'https://neobissite.firebaseio.com/';


const app = (

        <Provider store={store}>
            {/*<BrowserRouter><App /> </BrowserRouter>*/}
            <App/>
        </Provider>

);

ReactDOM.render(app, document.getElementById('root'));
registerServiceWorker();
