import * as actionTypes from './actionTypes';
import axios from 'axios';

export const menuRequest = () => ({type: actionTypes.MENU_REQUEST});
export const menuSuccess = (menuItems) => ({type: actionTypes.MENU_SUCCESS, menuItems});
export const menuFailure = error => ({type: actionTypes.MENU_FAILURE, error});

export const getMenu = () => {
    return (dispatch, getState) => {
        dispatch(menuRequest());
        axios.get('/menu.json').then(response => {
            dispatch(menuSuccess(response.data));
        }, error => {
            dispatch(menuFailure(error));
        })
    }
}

