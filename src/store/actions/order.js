import * as actionTypes from '../actions/actionTypes';
// import axios from 'axios';

export const orderItem = (key, name) => ({type: actionTypes.ORDER_ITEM, key, name});

export const reduceOrderedItem = key => ({type: actionTypes.REDUCE_ORDERED_ITEM, key});

// export const orderRequest = () => ({type: actionTypes.ORDER_REQUEST});
// export const orderSuccess = () => ({type: actionTypes.ORDER_SUCCESS});
// export const orderFailure = (error) => ({type: actionTypes.ORDER_FAILURE, error });
//
// export const placeOrder = (contactData) => {
//     return (dispatch, getState) => {
//         dispatch(orderRequest());
//         const currentOrder = {
//             order: getState().order.order,
//             contactData
//         };
//         axios.post('/orders.json', currentOrder).then(response => {
//             dispatch(orderSuccess());
//         }, error => {
//             dispatch(orderFailure(error));
//         })
//     }
// };