import * as actionTypes from '../actions/actionTypes';

const initialState = {
    menuItems: null,
    error: null,
    loading: false
    // ordered: false
};

const reducer = (state = initialState, action) => {
    switch(action.type) {
        case actionTypes.MENU_REQUEST:
            return {...state, loading: true};
        case actionTypes.MENU_SUCCESS:
            return {...state, menuItems: action.menuItems, loading: false};
        case actionTypes.MENU_FAILURE:
            return {...state, error: action.error, loading: false};
        default:
            return state;
    }
};

export default reducer;