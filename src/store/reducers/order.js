import * as actionTypes from '../actions/actionTypes';

const initialState = {
    order: {}
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.ORDER_ITEM:
            if (Object.keys(state.order).includes(action.key)) {
                return {
                    order: {
                        ...state.order,
                        [action.key]: {
                            ...state.order[action.key],
                            amount: state.order[action.key].amount + 1
                        }

                    }
                }
            } else {
                return {
                    order: {
                        ...state.order,
                        [action.key]: {
                            name: action.name,
                            amount: 1
                        }
                    }
                };
            }
        case actionTypes.REDUCE_ORDERED_ITEM:
            if (state.order[action.key].amount !== 0) {
                return {
                    order: {
                        ...state.order,
                        [action.key]: {
                            ...state.order[action.key],
                            amount: state.order[action.key].amount - 1
                        }

                    }

                }
            } else {
                delete state.order[action.key];
                return state;
            }
        case actionTypes.ORDER_SUCCESS:
            return {
                ...state,
                contactData: action.contactData
            }
        default:
            return state;
    }
};

export default reducer;
